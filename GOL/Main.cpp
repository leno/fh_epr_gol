#include  "Main.h"

int main(int argc, char* argv[]){
	if(!startParameterinput(argc,argv)){
		cin.get();
		return -1;
	}
	Timer generalTimer;
	generalTimer.start();
	switch (parallelMethode){
		case 0: new GOL_OMP(inputFilePath, outputFilePath, genCount, enableMeasurement);break;
		case 1: new GOL_OCL(inputFilePath, outputFilePath, genCount, enableMeasurement);break;
		case 2: new GOL_SEQ(inputFilePath, outputFilePath, genCount, enableMeasurement);break;
		default: cout << "no parallel Methode Selected";break;
	}
	generalTimer.stop();
	cout << "general Time: " << generalTimer.getElapsedTimeInSec();
	cin.get();
	return 0;
}

//handles the parameter input
bool startParameterinput(int argc, char* argv[]){
	//cout << "--load \"Filename\" "<<"--save \"Filename\" " << "--generations generationCount "<<"--measure" << "--omp || --ocl || --seq \n";
	for(int i = 1; i < argc; i++) { //iterate over argv
		if ( (strcmp(argv[i], "--load") == 0) && (i+1 < argc)){
			  inputFilePath = argv[i+1];
			  i++;
		}else if ( (strcmp(argv[i], "--save") == 0) && (i+1 < argc)){
			  outputFilePath = argv[i+1];
			  i++;
		}else if ( (strcmp(argv[i], "--generations") == 0) && (i+1 < argc)){
			  genCount = stoi(argv[i+1]);
			  i++;
		}else if( (strcmp(argv[i], "--omp") == 0)){
			  cout << "openmp \n";
			  parallelMethode = 0;
		}else if( (strcmp(argv[i], "--ocl") == 0)){
			  cout << "opencl \n";
			  parallelMethode = 1;
		}else if( (strcmp(argv[i], "--seq") == 0)){
			  cout << "sequenziell \n";
			  parallelMethode = 2;
		}else if ( (strcmp(argv[i], "--measure") == 0)){
			  enableMeasurement = true;
			  i++;
		}else{cout << " \n some parameters seem wrong \n";}
	}
	if(inputFilePath.empty() || outputFilePath.empty() || genCount < 1){
		return false;
	}else{
		return true;
	}
}