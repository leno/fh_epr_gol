#include "GOL_SEQ.h"

GOL_SEQ::GOL_SEQ(std::string inputFilePath, std::string outputFilePath, int genCount, bool enableMeasurement) : GOL(inputFilePath, genCount, enableMeasurement){
	kernelTimer.start();
	calcGenerations(genCount);
	kernelTimer.stop();
	setAndWriteData(outputFilePath);
	finalizeTimer.start();
	deleteResources();
	finalizeTimer.stop();
	displayTimes();
}

//calculates one generation
void GOL_SEQ::calcGeneration(int* loadArray, int* saveArray){
	for(int i = 1; i < colums - 1; i++){
		for(int j = 1; j < rows - 1; j ++){
			saveArray[i*colums+j] = checkField(loadArray,i*colums+j);
		}
	}
	dataIO.fillOffset(saveArray);
}