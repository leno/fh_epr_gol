#include "stdafx.h"
#include "DataIO.h"
#include "GOL_OMP.h"
#include "GOL_OCL.h"
#include "GOL_SEQ.h"

using namespace std;

bool startParameterinput(int,char*[]);
string inputFilePath;
string outputFilePath;
int genCount;
bool enableMeasurement;
int parallelMethode;