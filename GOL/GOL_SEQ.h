#ifndef GOL_SEQ_H_DEF
#define GOL_SEQ_H_DEF

#include "stdafx.h"
#include "GOL.h"

class GOL_SEQ: public GOL{
private:

public:
	GOL_SEQ(const string, const string, int, bool);
	void calcGeneration(int*,int*);
};
#endif