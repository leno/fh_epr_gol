__kernel void borderRows(const int width,
						const int height,
                        __global int *grid)
{
    int id = get_global_id(0) + 1;
 
    if (id < width-1)
    {
        grid[width*(height-1)+id] = grid[width+id]; //Copy first real row to bottom row
        grid[id] = grid[width*(height-2) + id]; //Copy last real row to top row
    }
}
 
__kernel void borderCols(const int width,
						const int height,
                        __global int *grid)
{
    int id = get_global_id(0);
 
    if (id < height)
    {
		grid[id*width+(width-1)] = grid[id*width+1]; //Copy first real column to right most column
        grid[id*width] = grid[id*width + (width-2)]; //Copy last real column to left most column
    }
}
 
__kernel void GOL(const int width,
				  const int height,
                  __global int *grid,
                  __global int *newGrid)
{
    int ix = get_global_id(0) + 1;
    int iy = get_global_id(1) + 1;
    int id = iy * width + ix;
 
    int numNeighbors;
 
    if (iy <= height-2 && ix <= width-2) {
 
		numNeighbors = grid[id+width] + grid[id-width]
					 + grid[id+1] + grid[id-1]             
					 + grid[id+width+1] + grid[id-width-1] 
					 + grid[id-width+1] + grid[id+width-1];
 
		int cell = grid[id];

		if((cell == 0) && (numNeighbors == 3)){
			newGrid[id] = 1;
		} else if ((cell == 1) && ((numNeighbors > 3) || (numNeighbors < 2)))
		{
			newGrid[id] = 0;
		} else {
			newGrid[id] = cell;
		}
    }
}