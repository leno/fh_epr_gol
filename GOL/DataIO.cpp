#include "DataIO.h"

DataIO::DataIO(){

}

DataIO::~DataIO(){

}
//reads data from a file into the loadedData Arr
void DataIO::inputData(string path){
	
	int valueCounter = 0;
	string line;
	ifstream golFile(path);
	if (golFile.is_open()){
		getline(golFile,line);
		firstLineToArray(line);

		//offset erste Reihe
		for(int i = 0; i < colums; i++){
			//loadedData[i] = 0;
		}
		//cout << "first offset complete \n";
		valueCounter = colums;

		while(getline(golFile,line)){
			loadedData[valueCounter] = 0;//Ghost vor der Reihe
			valueCounter ++;
			for(string::iterator it = line.begin(); it != line.end(); ++it){
				loadedData[valueCounter] = charToInt(*it);
				valueCounter ++;
			}
			loadedData[valueCounter] = 0;//Ghost nach der Reihe
			valueCounter ++;
		}

		//cout << "real values complete \n";

		//offset letzte Reihe
		for(int i = valueCounter;i < rows * colums; i ++){
			loadedData[i] = 0;
		}
		//cout << "last offset complete \n";
		golFile.close();
		fillOffset(loadedData);
		//cout << "loading complete \n";
    }else{  
		//cout << "Unable to open file"; 
    }
}

void DataIO::fillOffset(int* dataArr){
	//cout << "rows: " << rows << ";" << "colums: " << colums << "\n";

	//Copy Rows to Ghost
	for( int i = 1; i < colums - 1; i++){
		dataArr[(rows*(colums - 1) - 1) + i] = dataArr[colums + i]; //Copy first real row to bottom ghost row
        dataArr[i] = dataArr[((rows - 2)*colums) + i]; //Copy last real row to top ghost row		
	}
	//cout << "Rows to Ghost complete \n";
	//Copy corner to Ghost
	dataArr[0] = dataArr[(rows - 1) * colums - 2];// Copy real bot right corner to ghost top left
	dataArr[colums - 1] = dataArr[(rows - 1) * colums - 2];// Copy real bot left corner to ghost top right
	dataArr[rows * colums - 1] = dataArr[colums + 1];// Copy real top left corner to ghost bot right
	dataArr[(rows - 1) * colums] = dataArr[2*colums - 2];// Copy real top right corner to ghost bot left
	//cout << "corners to Ghost complete \n";

	// Copy Colums to Ghost
	for(int i = 0; i < rows; i++){
		//cout << dataArr[i * colums  + colums -2];
	   dataArr[i*colums + (colums - 2)] = dataArr[i * colums  + 1]; //Copy first real column to right most ghost column <-------falsch
	   dataArr[i*colums] = dataArr[i*colums  + colums - 3]; //Copy last real column to left most ghost column			<-------falsch
	}
	//cout << "colums to Ghost complete \n";
		//cout << "\n";
}


// writes data to file, data must be set first
void DataIO::outputData(string path){
	// Open for write
	ofstream golFile(path);
	if(golFile.is_open()){
		golFile << colums-2 << "," << rows-2 << "\n";
		for( int i = 1; i < rows - 1; i++){
			for(int j = 1; j < colums - 1; j++){
				golFile << intToChar(savedData[i*colums + j]);
			}
			golFile << "\n";
			
		}
		golFile.close();
	}else{
		cout << "Unable to open save file"; 
	}
}


//get data from reading you need to read first
int* DataIO::getData(){
	return loadedData;
}

//set data for writing 
void DataIO::setData(int* boolArray){
	savedData = boolArray;
}

//reads the first line from the input file and creates the loadedData Array
void DataIO::firstLineToArray(string firstLine){
	
	string delimiter = ",";
	string characters = firstLine.substr(0, firstLine.find(delimiter));
	string lines = firstLine.substr(firstLine.find(delimiter) +1,firstLine.size() - firstLine.find(delimiter));
	//cout << characters << ",\n" << lines; 
	colums = stoi(characters) + 2;
	rows = stoi(lines) + 2;
	loadedData = new int[rows*colums];
	cout << "first row complete \n";
}

int DataIO::charToInt(char ch){
	
	if(ch == 'x'){
		return 1;
	}else return 0;
}

char DataIO::intToChar(int b){
	if(b == 1){
		return 'x';
	}else{
		return '.';
	}
}