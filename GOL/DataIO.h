#ifndef DATAIO_H
#define DATAIO_H

#include <fstream>
#include <iostream>
#include "stdafx.h"
using namespace std;
//template<size_t RowsN, size_t ColumsN>
class DataIO{
	public:
		DataIO();
		~DataIO();

		void inputData(string);
		void outputData(string);
		int* getData();
		void setData(int*);
		int rows;
		int colums;
		void fillOffset(int*);
	private:
		int* loadedData;
		int* savedData;
		void firstLineToArray(string);
		int charToInt(char);
		char intToChar(int);
};
#endif