#ifndef GOL_H_DEF
#define GOL_H_DEF

#include "stdafx.h"
#include "DataIO.h"
#include "Timer.h"
using namespace std;

class GOL{
private:
	bool enableMeasurement;
	
	int genCount;

	void initDataArr2();

	void displayTime(Timer timer);
	int checkUp(int*, int);
	int checkDown(int*, int);
	int checkLeft(int*, int);
	int checkRight(int*, int);
	int checkUpLeft(int*, int);
	int checkUpRight(int*, int);
	int checkDownLeft(int*, int);
	int checkDownRight(int*, int);

protected:
	DataIO dataIO;
	Timer initTimer;
	Timer kernelTimer;
	Timer finalizeTimer;

	void displayTimes();

	void deleteResources();
	void calcGenerations(int);
	void setAndWriteData(string);
	virtual void calcGeneration(int*, int*) = 0;
	int* dataArr1;
	int* dataArr2;

	int rows;
	int colums;

	int checkField(int*, int);
public :
	GOL(const string, int, bool);
};
#endif