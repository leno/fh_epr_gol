#include "GOL_OCL.h"
std::string GetPlatformName (cl_platform_id id);
std::string GetDeviceName (cl_device_id id);

GOL_OCL::GOL_OCL(std::string inputFilePath, std::string outputFilePath, int genCount, bool enableMeasurement) : GOL(inputFilePath, genCount, enableMeasurement){
	init();
	kernelTimer.start();
	calcGeneration(nullptr, nullptr);
	kernelTimer.stop();
	setAndWriteData(outputFilePath);
	finalizeTimer.start();
	deleteResources();
	finalizeTimer.stop();
	displayTimes();
}

bool GOL_OCL::init(){
	width = colums;
	height = rows;
	primaryField = dataArr1;
	secondaryField = dataArr2;
	/*
	*
	*   OPENCL BEGIN
	**
	*/

	cl_int err;
	int local_size = 16;

	bytes = sizeof(int)*(rows)*(colums);

	cl_uint platformIdCount = 0;
	clGetPlatformIDs (0, nullptr, &platformIdCount);

	if (platformIdCount == 0) {
		std::cerr << "No OpenCL platform found" << std::endl;
		return 1;
	}

	std::vector<cl_platform_id> platformIds (platformIdCount);
	clGetPlatformIDs (platformIdCount, platformIds.data (), nullptr);

	cl_uint deviceIdCount = 0;
	clGetDeviceIDs (platformIds [0], CL_DEVICE_TYPE_GPU, 0, nullptr,
	&deviceIdCount);

	if (deviceIdCount == 0) {
		std::cerr << "No OpenCL devices found" << std::endl;
		return 1;
	}

	std::vector<cl_device_id> deviceIds (deviceIdCount);
	clGetDeviceIDs (platformIds [0], CL_DEVICE_TYPE_GPU, deviceIdCount,
	deviceIds.data (), nullptr);

	device_id = deviceIds.front();
/*
	//-------------------------------------------------
cl_uint platformIdCount = 0;
 clGetPlatformIDs (0, nullptr, &platformIdCount);

 if (platformIdCount == 0) {
  std::cerr << "No OpenCL platform found" << std::endl;
  return 1;
 } else {
  std::cout << "Found " << platformIdCount << " platform(s)" << std::endl;
 }

 std::vector<cl_platform_id> platformIds (platformIdCount);
 clGetPlatformIDs (platformIdCount, platformIds.data (), nullptr);

 for (cl_uint i = 0; i < platformIdCount; ++i) {
  std::cout << "\t (" << (i+1) << ") : " << GetPlatformName (platformIds [i]) << std::endl;
 }

 // http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clGetDeviceIDs.html
 cl_uint deviceIdCount = 0;
 clGetDeviceIDs (platformIds [1], CL_DEVICE_TYPE_ALL, 0, nullptr,
  &deviceIdCount);

 if (deviceIdCount == 0) {
  std::cerr << "No OpenCL devices found" << std::endl;
  return 1;
 } else {
  std::cout << "Found " << deviceIdCount << " device(s)" << std::endl;
 }

 std::vector<cl_device_id> deviceIds (deviceIdCount);
 clGetDeviceIDs (platformIds [1], CL_DEVICE_TYPE_ALL, deviceIdCount,
  deviceIds.data (), nullptr);

 for (cl_uint i = 0; i < deviceIdCount; ++i) {
  std::cout << "\t (" << (i+1) << ") : " << GetDeviceName (deviceIds [i]) << std::endl;
 }

 //----------------------------------------------------------
 */
	// Create a context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context) {
		printf("Error: Failed to create a compute context\n");
		return EXIT_FAILURE;
	}
 
	// Create a command queue
	queue = clCreateCommandQueue(context, device_id, 0, &err);
	if (!queue) {
		printf("Error: Failed to create a command commands\n");
		return EXIT_FAILURE;
	}
 
	// Create the compute program from the kernel source file
	char *fileName = "GoL_Kernel.cl";

	std::ifstream in (fileName);
	std::string source (
		(std::istreambuf_iterator<char> (in)),
		std::istreambuf_iterator<char> ());

	size_t lengths [1] = { source.size () };
	const char* sources [1] = { source.data () };

	cl_int error = 0;
	cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &err);
	if (!program || err != CL_SUCCESS) {
		printf("Error: Failed to create compute program\n");
		return EXIT_FAILURE;
	}

	// Build the program executable
	err = clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);
	if (err != CL_SUCCESS) {
		printf("Error: Failed to build program executable %d\n", err);
		return EXIT_FAILURE;
	}

	// Create the GOL kernel in the program we wish to run
	k_gol = clCreateKernel(program, "GOL", &err);
	if (!k_gol || err != CL_SUCCESS) {
		printf("Error: Failed to create GOL kernel \n");
	return EXIT_FAILURE;
	}
 
	// Create the ghostRows kernel in the program we wish to run
	k_borderRows = clCreateKernel(program, "borderRows", &err);
	if (!k_borderRows || err != CL_SUCCESS) {
		printf("Error: Failed to create borderRows kernel\n");
	return EXIT_FAILURE;
	}
 
	// Create the ghostCols kernel in the program we wish to run
	k_borderCols = clCreateKernel(program, "borderCols", &err);
	if (!k_borderCols || err != CL_SUCCESS) {
		printf("Error: Failed to create borderCols kernel\n");
	return EXIT_FAILURE;
	}

	// Create the input and output arrays in device memory for our calculation
	d_grid = clCreateBuffer(context, CL_MEM_READ_WRITE, bytes, NULL, NULL);
	d_newGrid = clCreateBuffer(context, CL_MEM_READ_WRITE, bytes, NULL, NULL);
	if (!d_grid || !d_newGrid) {
		printf("Error: Failed to allocate device memory\n");
		return EXIT_FAILURE;
	}

	// Write our data set into the input array in device memory
	err = clEnqueueWriteBuffer(queue, d_grid, CL_TRUE, 0,
		bytes, primaryField, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printf("Error: Failed to write to source array\n");
		return EXIT_FAILURE;
	}

	// Set the arguments to ghostRows kernel
    err  = clSetKernelArg(k_borderRows, 0, sizeof(int), &width);
	err |= clSetKernelArg(k_borderRows, 1, sizeof(int), &height);
    err |= clSetKernelArg(k_borderRows, 2, sizeof(cl_mem), &d_grid);
    if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments\n");
      return EXIT_FAILURE;
    }

	// Set the arguments to ghostCols kernel
    err  = clSetKernelArg(k_borderCols, 0, sizeof(int), &width);
	err |= clSetKernelArg(k_borderCols, 1, sizeof(int), &height);
    err |= clSetKernelArg(k_borderCols, 2, sizeof(cl_mem), &d_grid);
    if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments\n");
      return EXIT_FAILURE;
    }

	// Set the arguments to GOL  kernel
    err  = clSetKernelArg(k_gol, 0, sizeof(int), &width);
	err |= clSetKernelArg(k_gol, 1, sizeof(int), &height);
    err |= clSetKernelArg(k_gol, 2, sizeof(cl_mem), &d_grid);
    err |= clSetKernelArg(k_gol, 3, sizeof(cl_mem), &d_newGrid);
    if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments\n");
      return EXIT_FAILURE;
    }

	// Set kernel local and global sizes
	cpyLocalSize = local_size;
	// Number of total work items - localSize must be devisor
	cpyRowsGlobalSize = (size_t)ceil(width/(float)cpyLocalSize)*cpyLocalSize;
	cpyColsGlobalSize = (size_t)ceil(height/(float)cpyLocalSize)*cpyLocalSize;
 
	GolLocalSize[0] = local_size;
	GolLocalSize[1] = local_size;
	linGlobalW = (size_t)ceil(width/(float)local_size)*local_size;
	linGlobalH = (size_t)ceil(height/(float)local_size)*local_size;
	GolGlobalSize[0] = linGlobalW;
	GolGlobalSize[1] = linGlobalH;

	/*
	*
	*   OPENCL END
	*
	*/
	return true;
}


//calculates one generation
void GOL_OCL::calcGeneration(int* loadArray, int* saveArray){
	cl_int err;

	// Main game loop
    for (int iter = 0; iter<250; iter++) {
        err  = clEnqueueNDRangeKernel(queue, k_borderRows, 1, NULL, &cpyRowsGlobalSize, &cpyLocalSize, 0, NULL, NULL);
		err |= clEnqueueNDRangeKernel(queue, k_borderCols, 1, NULL, &cpyColsGlobalSize, &cpyLocalSize, 0, NULL, NULL);
        err |= clEnqueueNDRangeKernel(queue, k_gol, 2, NULL, GolGlobalSize, GolLocalSize, 0, NULL, NULL);
 
        if(iter%2 == 1) {
            err |= clSetKernelArg(k_borderRows, 2, sizeof(cl_mem), &d_grid);
            err |= clSetKernelArg(k_borderCols, 2, sizeof(cl_mem), &d_grid);
            err |= clSetKernelArg(k_gol, 2, sizeof(cl_mem), &d_grid);
            err |= clSetKernelArg(k_gol, 3, sizeof(cl_mem), &d_newGrid);
        } else {
            err |= clSetKernelArg(k_borderRows, 2, sizeof(cl_mem), &d_newGrid);
            err |= clSetKernelArg(k_borderCols, 2, sizeof(cl_mem), &d_newGrid);
            err |= clSetKernelArg(k_gol, 2, sizeof(cl_mem), &d_newGrid);
            err |= clSetKernelArg(k_gol, 3, sizeof(cl_mem), &d_grid);
        }
    }

	if (err != CL_SUCCESS) {
       printf("Error: Failed to launch kernels%d\n",err);
       return;
    }

	// Wait for the command queue to get serviced before reading back results
    clFinish(queue);
 
    // Read the results from the device
    err =  clEnqueueReadBuffer(queue, d_grid, CL_TRUE, 0,
		bytes, primaryField, 0, NULL, NULL );
    if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array\n");
      return;
    }
}

// Deletes the two arrays. Is used instead of the destructor, because
// the time of deleting has to be measured
void GOL_OCL::freeResources(){
	delete[] primaryField;
	delete[] secondaryField;

	clReleaseMemObject (d_grid);
	clReleaseMemObject (d_newGrid);

	clReleaseCommandQueue (queue);
	
	clReleaseKernel (k_gol);
	clReleaseKernel (k_borderCols);
	clReleaseKernel (k_borderRows);
	clReleaseProgram (program);

	clReleaseContext (context);
}

std::string GOL_OCL::getPlatformName(cl_platform_id id) {
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, nullptr, &size);

	std::string result;
	result.resize (size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size,
		const_cast<char*> (result.data ()), nullptr);

	return result;
}

std::string GOL_OCL::getDeviceName(cl_device_id id) {
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, nullptr, &size);

	std::string result;
	result.resize (size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,
		const_cast<char*> (result.data ()), nullptr);

	return result;
}

void GOL_OCL::checkError(cl_int error, std::string msg) {
	if (error != CL_SUCCESS) {
		std::cerr << "OpenCL call failed with error " << error << std::endl;
		std::cerr << msg << std::endl;
		std::cin.get();
		std::exit (1);
	}
}

std::string GOL_OCL::loadKernel (const char* name)
{
	std::ifstream in (name);
	std::string result (
		(std::istreambuf_iterator<char> (in)),
		std::istreambuf_iterator<char> ());
	return result;
}

cl_program GOL_OCL::createProgram (const std::string& source, cl_context context)
{
	size_t lengths [1] = { source.size () };
	const char* sources [1] = { source.data () };

	cl_int error = 0;
	cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
	checkError (error, "clCreateProgramWithSource error");

	return program;
}

std::string GetPlatformName (cl_platform_id id)
{
 size_t size = 0;
 clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, nullptr, &size);

 std::string result;
 result.resize (size);
 clGetPlatformInfo (id, CL_PLATFORM_NAME, size,
  const_cast<char*> (result.data ()), nullptr);

 return result;
}

std::string GetDeviceName (cl_device_id id)
{
 size_t size = 0;
 clGetDeviceInfo (id, CL_DEVICE_NAME, 0, nullptr, &size);

 std::string result;
 result.resize (size);
 clGetDeviceInfo (id, CL_DEVICE_NAME, size,
  const_cast<char*> (result.data ()), nullptr);

 return result;
}