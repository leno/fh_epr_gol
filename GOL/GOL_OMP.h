#ifndef GOL_OMP_H_DEF
#define GOL_OMP_H_DEF

#include "stdafx.h"
#include "GOL.h"

class GOL_OMP: public GOL{
private:

public:
	GOL_OMP(const string, const string, int, bool);
	void calcGeneration(int*,int*);
};
#endif