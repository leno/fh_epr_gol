// GOL.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include"GOL.h"

GOL::GOL(const string inputFilePath, int genCount, bool enableMeasurement){
	/*enableMeasurement = true;
	genCount = 1;
	inputFilePath = "random10000_in.gol";
	outputFilePath = "output.gol";*/
	initTimer.start();
		dataIO.inputData(inputFilePath);
			
		//Global definition
		enableMeasurement = enableMeasurement;
		genCount = genCount;
		rows = dataIO.rows;
		colums = dataIO.colums;

		//Get read data from DataIO and init 
		dataArr1 = dataIO.getData();
		initDataArr2();
		initTimer.stop();
}

void GOL::setAndWriteData(string outputFilePath){
		// If the generation Count is even, the second array writen
		if(genCount / 2 == 0){
			dataIO.setData(dataArr2);
		}else{
			dataIO.setData(dataArr1);
		}
		dataIO.outputData(outputFilePath);
}

void GOL::displayTimes(){
	if(enableMeasurement){
		displayTime(initTimer);
		displayTime(kernelTimer);
		displayTime(finalizeTimer);
	}
}

//time output to console
void GOL::displayTime(Timer timer){
		cout << "Time Elapsed: " << static_cast<int>(timer.getElapsedTimeInSec()) / 3600<< ":" <<static_cast<int>(timer.getElapsedTimeInSec()) /60<< ":" << static_cast<int>(timer.getElapsedTimeInSec()) << "." << static_cast<int>(timer.getElapsedTimeInMicroSec()/1000) <<"\n";
}



// creates a second Data Array for saving
void GOL::initDataArr2(){
	dataArr2 = new int[rows * colums];
}

//calculates all generations
void GOL::calcGenerations(int generationCount){
	for(int generations = 0; generations < generationCount; generations ++){
		if(generations % 2 == 0){
			calcGeneration(dataArr1, dataArr2); // if even, data is written into dataArr2
		}else{
			calcGeneration(dataArr2, dataArr1); // if odd, data is written into dataArr1
		}
	}
}

// Checks one field in the loaded Array
int GOL::checkField(int* loadArray, int i){
	int activeNeighboursCount = 0;
	activeNeighboursCount += checkUp(		 loadArray,i);
	activeNeighboursCount += checkDown(		 loadArray,i);
	activeNeighboursCount += checkRight(	 loadArray,i);
	activeNeighboursCount += checkLeft(		 loadArray,i);
	activeNeighboursCount += checkUpLeft(	 loadArray,i);
	activeNeighboursCount += checkUpRight(	 loadArray,i);
	activeNeighboursCount += checkDownLeft(	 loadArray,i);
	activeNeighboursCount += checkDownRight( loadArray,i);

	if(loadArray[i] == 1 && (activeNeighboursCount  < 2 ||activeNeighboursCount > 3)){
		return 0;
	}else return 1;

	if(loadArray[i] == 0 && activeNeighboursCount == 3) return 1;
		else return 0;
}

int GOL::checkUp(int* loadArray, int i){
	return loadArray[i - colums ];
}

int GOL::checkDown(int* loadArray, int i){
		return loadArray[i + colums];
}

int GOL::checkRight(int* loadArray, int i){
		return loadArray[i + 1];
}

int GOL::checkLeft(int* loadArray, int i){
		return loadArray[i - 1];
}

int GOL::checkUpLeft(int* loadArray, int i){
		return loadArray[i - colums - 1];
}

int GOL::checkUpRight(int* loadArray, int i){
		return loadArray[i - colums + 1];
}

int GOL::checkDownLeft(int* loadArray, int i){
		return loadArray[i + colums - 1];
}

int GOL::checkDownRight(int* loadArray, int i){
		return loadArray[i + colums + 1];
}

void GOL::deleteResources(){
 delete[] dataArr1;
 delete[] dataArr2;
}