#include "GOL_OMP.h"

GOL_OMP::GOL_OMP(const std::string inputFilePath, const std::string outputFilePath, int genCount, bool enableMeasurement) : GOL(inputFilePath, genCount, enableMeasurement){
	kernelTimer.start();
	calcGenerations(genCount);
	kernelTimer.stop();
	setAndWriteData(outputFilePath);
	finalizeTimer.start();
	deleteResources();
	finalizeTimer.stop();
	displayTimes();
}

//calculates one generation
void GOL_OMP::calcGeneration(int* loadArray, int* saveArray){
	//cout << "start calc generation \n";
	#pragma omp parallel
	{
		#pragma omp for
	
	for(int ij = colums + 1; ij < ((rows - 2)*colums + colums - 2); ij ++){
					
			int i = ij/colums;
			int j = ij%colums;
			if(j == 0 || j == colums -1){

				}else{
					//cout << colums << "\n";
					//cout << "i: " << i << " ; " << "j: " << j << " ; " << "arrPos: " <<i*colums+j << " ; " << "ArrPosVal: " << "saveArray[i*rows+j]" << "\n";
					saveArray[i*colums+j] = checkField(loadArray,i*colums+j);
				}
			}	
	}
	dataIO.fillOffset(saveArray);
}