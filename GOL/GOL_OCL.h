#ifndef GOL_OCL_H_DEF
#define GOL_OCL_H_DEF

#include "stdafx.h"
#include "GOL.h"
#include <CL\cl.h>
#include <sys/stat.h>
#include <vector>

class GOL_OCL: public GOL{
private:
	bool init();



	cl_platform_id cpPlatform;
    cl_device_id device_id;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
	cl_kernel k_gol, k_borderRows, k_borderCols;

	cl_mem d_grid;
    cl_mem d_newGrid;


	size_t cpyRowsGlobalSize, cpyColsGlobalSize, cpyLocalSize;
	size_t GolLocalSize[2];
	size_t linGlobalW;
	size_t linGlobalH;
	size_t GolGlobalSize[2];
	size_t bytes;
public:
	GOL_OCL(const string, const string, int, bool);
	void calcGeneration(int*,int*);

	int width;					
	int height;					

	int *primaryField;			
	int *secondaryField;		

	std::string getPlatformName(cl_platform_id id);
	std::string getDeviceName(cl_device_id id);
	void checkError(cl_int error, std::string msg);
	std::string loadKernel (const char* name);
	cl_program createProgram (const std::string& source, cl_context context);

	void freeResources();
};
#endif